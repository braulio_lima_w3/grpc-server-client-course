package br.com.neppo.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class DemoController {

    @Autowired
    private ProtoClient client;

    @GetMapping("/")
    public ResponseEntity<String> hello(){
        return ResponseEntity.ok("Hello world");
    }

    @GetMapping("/fibonacci")
    public ResponseEntity<Number> fib(@RequestParam(name = "number", required = false) Integer max, @RequestParam(name = "recursive", required = false) Boolean recursive){
        if(max == null) max = 10;
        if(recursive == null) recursive = false;
        return ResponseEntity.ok(client.fibonacci(max, recursive));
    }

    @GetMapping("/random")
    public ResponseEntity<String> randomLine(){
        return ResponseEntity.ok(client.phrase());
    }


}
