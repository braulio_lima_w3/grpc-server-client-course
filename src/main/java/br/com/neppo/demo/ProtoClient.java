package br.com.neppo.demo;

import br.com.neppo.demo.DemoServiceGrpc;
import br.com.neppo.demo.GrpcFibonacci;
import com.google.protobuf.Empty;
import io.grpc.Channel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class ProtoClient {

    private Channel channel = null;
    private DemoServiceGrpc.DemoServiceBlockingStub client;

    @PostConstruct
    private void init(){
        Channel channel = ManagedChannelBuilder.forAddress("localhost", 50051).usePlaintext().build() ;
        this.client = DemoServiceGrpc.newBlockingStub(channel);
    }

    public int fibonacci(int to, boolean recur){
        return client.calculateFibonacci( GrpcFibonacci.newBuilder().setRecusrive(recur).setIterations(to).build() ).getAnswer();
    }

    public String phrase(){
        return client.random(Empty.newBuilder().build()).getAnswer();
    }

}
